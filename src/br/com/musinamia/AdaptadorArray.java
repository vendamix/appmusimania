package br.com.musinamia;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class AdaptadorArray extends ArrayAdapter<Post> {
	
	  private final Context context;
	  private final List<Post> values;

	  public AdaptadorArray(Context context, List<Post> result) {
	    super(context, R.layout.single_row2, result);
	    this.context = context;
	    this.values = result;
	  }

	  @Override
	  public View getView(int position, View convertView, ViewGroup parent) {
	    LayoutInflater inflater = (LayoutInflater) context
	        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	    View rowView = inflater.inflate(R.layout.single_row2, parent, false);
	    TextView textView = (TextView) rowView.findViewById(R.id.title);
	    ImageView imageView = (ImageView) rowView.findViewById(R.id.icon);
	    TextView descricaoView = (TextView) rowView.findViewById(R.id.secondLine);
	    
	    
	    Post post = values.get(position);
	    
	   
		try {
			
			Context contexto = getContext();
			
			Bitmap image = Utilidades.downloadBitmap(post.getArquivoMiniPosition(0),contexto);
			
			if(!image.equals("") && image != null){
				imageView.setImageBitmap(image);
			}
		    
			
		} catch (Exception e) {
			
			Log.e("Sytesk", "Falha ao carregar imagem "+post.getArquivoMiniPosition(0)+" ,detalhes: "+e.getMessage());
		}
	    
	    
	    textView.setText(Html.fromHtml(post.getTitulo()));
	    descricaoView.setText(Html.fromHtml(post.getTruncate()));
	   

	    return rowView;
	  }
	  
	 

}
