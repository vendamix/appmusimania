package br.com.musinamia;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.util.Log;


public class Utilidades {
	
	/* Fun��o para verificar exist�ncia de conex�o com a internet 
	*/  
	public static  boolean verificaConexao(Context context) {  
		
	    boolean conectado;  
	    ConnectivityManager conectivtyManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);  
	    if (conectivtyManager.getActiveNetworkInfo() != null  
	            && conectivtyManager.getActiveNetworkInfo().isAvailable()  
	            && conectivtyManager.getActiveNetworkInfo().isConnected()) {  
	        conectado = true;  
	    } else {  
	        conectado = false;  
	    }  
	    return conectado;  
	} 
	
	public static Bitmap downloadBitmap(String url,Context context) {
	     
		  Bitmap image = null;
		  
		  
		  
	     try {
	    	 
	    	 String[] aString = url.split("/");
				
			  int ref = (aString.length -1);
			  
			  String nomeArquivo = aString[ref];
			  
			  String cacheDir = context.getFilesDir().toString();
			  
			  String ponteiroArquivo = cacheDir+"/"+nomeArquivo;
			  
			  File chDir = new File(cacheDir); 
			  
			  if(!chDir.exists()){
				  chDir.mkdir();
			  }
			  
			  
			  
			  File arq = new File(ponteiroArquivo); 
			  
			  boolean existeArquivo = arq.exists();
			  
	    	 
	    	 InputStream input = null;
	    	
	    	 
	    	  if(!existeArquivo){
	    		  
	    		  URL aURL = new URL(url);
	 	    	 
		          HttpURLConnection connection = (HttpURLConnection) aURL.openConnection();
		          
		          connection.setDoInput(true);
		          
		          connection.connect();
		          
		          input = connection.getInputStream();
		          
		          image = BitmapFactory.decodeStream(input);
		          
		          
		        	  FileOutputStream fos = new FileOutputStream(ponteiroArquivo); 
			          image.compress(Bitmap.CompressFormat.PNG, 12, fos);
			          
			          byte[] bytes = new byte[256];
			          int r = 0;
			          
			          do{
			        	  
			        	  r = input.read(bytes);
			        	  
			        	  if(r>=0){
			        		  fos.write(bytes,0,r);
			        	  }
			        	  
			          }while(r>=0);
			          
			           
			          fos.close();  
			          input.close();
		          
		          
	    	  }else{
	    		
	    		  BitmapFactory.Options options = new BitmapFactory.Options();
	    		  options.inPreferredConfig = Bitmap.Config.ARGB_8888;
	    		  image = BitmapFactory.decodeFile(ponteiroArquivo, options);
	    		  
	    	  }
	    	 
	          
	         
	          
	          
	          
       } catch (Exception e) {
          Log.e("Sytesk", "Error getting bitmap "+url+" "+e.getMessage(),e);
          
       }
	     
	     return image;
	    
	}
	
	public static String getConteudoArquivo(Context contexto,String nomeArquivo){
		
		StringBuffer sbBuffer = new StringBuffer();
		
		File arquivo;  
		
		String cacheDir = contexto.getFilesDir().toString();
		
		File chDir = new File(cacheDir); 
		
		if(!chDir.exists()){
			  chDir.mkdir();
		 }
		
		String ponteiroArquivo = cacheDir+"/"+nomeArquivo;
		Log.v("Sytesk", "getConteudo "+ponteiroArquivo);
		
		// Lendo do arquivo  
        arquivo = new File(ponteiroArquivo);  
        
        
        
        try {
			
        	 if(!arquivo.isFile()){
             	arquivo.createNewFile();
             }
             
             FileInputStream fis = new FileInputStream(arquivo);  
             
             
             
             int ln;  
             while ( (ln = fis.read()) != -1 ) { 
             	sbBuffer.append((char)ln);
             }  

             fis.close();  
        	 
             Log.v("Sytesk", "getConteudo > "+sbBuffer.toString());
             
		} catch (Exception e) {
			Log.e("Sytesk", "Falha ao tentar recuperar conteudo do arquivo "+nomeArquivo, e);
		}
        
		
		return sbBuffer.toString();
		
	}
	
	public static void escrecerArquivo(Context contexto,String nomeArquivo,String conteudo){
		
		File arquivo;  
		
		String cacheDir = contexto.getFilesDir().toString();
		
		File chDir = new File(cacheDir); 
		
		if(!chDir.exists()){
			  chDir.mkdir();
		 }
		
		String ponteiroArquivo = cacheDir+"/"+nomeArquivo;
		
		try {
			
			arquivo = new File(ponteiroArquivo); 
			
			if(!arquivo.isFile()){
             	arquivo.createNewFile();
             }
			
			FileOutputStream fos = new FileOutputStream(arquivo);  
	        fos.write(conteudo.getBytes());  
	        fos.close();  
			
		} catch (Exception e) {
			Log.e("Sytesk", "Falha ao tentar escrever arquivo "+nomeArquivo, e);
		}
		
		
	}
	
	public static String toString(InputStream is) throws IOException {

		byte[] bytes = new byte[1024];
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		int lidos;
		while ((lidos = is.read(bytes)) > 0) {
			baos.write(bytes, 0, lidos);
		}
		return new String(baos.toByteArray());
	}
	
	
}
