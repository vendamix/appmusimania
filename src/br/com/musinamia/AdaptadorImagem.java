package br.com.musinamia;

import java.util.List;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Gallery;
import android.widget.ImageView;


public class AdaptadorImagem extends BaseAdapter{
	private Context context;
	 
	
	private Post post;
	
	
	public void setPost(Post post) {
		this.post = post;
	}

	
	public AdaptadorImagem(Context c) {
		this.context = c;
	}
	
	
	public int getCount() {
		
		List<String> listaArquivos = post.getArquivo();
		
		return listaArquivos.size();
	}
	 
	public Object getItem(int position) {
		return position;
	}
	 
	public long getItemId(int position) {
		return position;
	}
	 

	public View getView(int position, View convertView, ViewGroup parent) {
	
	ImageView img = new ImageView(this.context);
	
	List<String> listaArquivos = post.getArquivo();
	
	String arquivoAtual =  listaArquivos.get(position);
	
	Bitmap image = Utilidades.downloadBitmap(arquivoAtual,this.context);
	
	img.setImageBitmap(image);
	
	img.setScaleType(ImageView.ScaleType.FIT_XY);
	//img.setLayoutParams(new Gallery.LayoutParams(165, 165));
	return img;
	}
	 
	public float getScale(boolean focused, int offset) {
		return Math.max(0, 1.0f / (float) Math.pow(2, Math.abs(offset)));
	}
}
