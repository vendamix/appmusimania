package br.com.musinamia;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.IBinder;
import android.os.StrictMode;
import android.text.Html;
import android.util.Log;


public class Notifica extends Service {
	
	
	
	public Notifica() {
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	@Deprecated
	public void onStart(Intent intent, int startId) {
		
		Log.v("Sytesk", "Iniciou servi�o");
		
		super.onStart(intent, startId);
	}
	
	public int onStartCommand(Intent intent, int flags, int startId) {
		
		Toaster toast = new Toaster(this);
		toast.start();
		
		
		 
		
	    return Service.START_NOT_STICKY;
	}
	
	public void lancaNotificacao(CharSequence mensagem,CharSequence titulo){
		
		
		NotificationManager notificationManager =
			    (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
		Notification notification = new Notification(R.drawable.document, mensagem, System.currentTimeMillis());
		
		Intent intent = new Intent(this, PopupActivity.class);
		
		Post postAtual = new Post();
		
		
		postAtual.setTitulo("Mensagem");
		
		List<String> arquivoList =  new ArrayList<String>();
		arquivoList.add("");
		postAtual.setArquivo(arquivoList);
		postAtual.setArquivoMini(arquivoList);
		String msg = Utilidades.getConteudoArquivo(this, "arquivo.txt");
		
		postAtual.setTexto(msg);
		
		
		Bundle b = new Bundle();
		b.putSerializable("postAtual", postAtual);
		intent.putExtras(b); 
		
		PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, 0);
		
		notification.flags |= Notification.FLAG_AUTO_CANCEL;
		
		notification.defaults |= Notification.DEFAULT_SOUND;
		
		notification.setLatestEventInfo(this, titulo,mensagem, pendingIntent);
		notificationManager.notify(R.string.app_name, notification);
		
	}
	
	private void verificaNotificacao(){
		
		 if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
         }
	        
         if(Utilidades.verificaConexao(this)){
        	 AvisoTask task = new AvisoTask();
        	 task.setContexto(this);
        	 task.execute();
         }
		
	}
	
	private class AvisoTask extends AsyncTask<String, Void, List<Post>>{
		
		private Context contexto;
		
		
		public void setContexto(Context contexto) {
			this.contexto = contexto;
		}


		@Override
		protected List<Post> doInBackground(String... params) {
			
			
			String siteURL = "http://www.musimania.com.br/mobile_aviso_token_b64bd5a1da6df03bb437d6e860e7ffc9a4c580e0";
			
			HttpClient httpclient = new DefaultHttpClient();
			HttpGet httpget = new HttpGet(siteURL);

			try {
				
				HttpResponse response = httpclient.execute(httpget);

				HttpEntity entity = response.getEntity();

				if (entity != null) {
					InputStream instream = entity.getContent();

					String json = Utilidades.toString(instream);
					instream.close();
					
					JSONObject jsonObject = new JSONObject(json);
	                
					
					String mensagem = (String) jsonObject.get("aviso2");
					
				
					String msgArquivo = Utilidades.getConteudoArquivo(contexto, "arquivo.txt");  
					
		            if(!msgArquivo.toString().equals(mensagem)){
		            	
		            	Utilidades.escrecerArquivo(contexto, "arquivo.txt", mensagem);
				        
				        if(!mensagem.isEmpty()){
		            		lancaNotificacao(Html.fromHtml(mensagem), "Musimania");
		            	}
		            }
		            
					
				}
				
			} catch (Exception e) {
				Log.e("Sytesk", "Falha ao acessar Web service", e);
			}
			return null;
		}
		
 }
	
	class Toaster extends Thread{
		
		Context contexto;
		
		public Toaster(Context contexto) {
			this.contexto = contexto;
		}
		
		@SuppressLint("ShowToast")
		public void run(){
			
			Notification note = new Notification( 0, null, System.currentTimeMillis() );
	        note.flags |= Notification.FLAG_NO_CLEAR;
	        startForeground( 1242, note );   
			
			while (true) {
				verificaNotificacao();
				Log.v("Sytesk", "Verificanco notificacao service...");
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e) {
					Log.e("Sytesk", "Erro no sleep",e);
					
				}
			}
			
			
			
		}
		
	}
	
}
