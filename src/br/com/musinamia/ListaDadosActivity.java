package br.com.musinamia;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

@SuppressLint("NewApi") public class ListaDadosActivity extends Activity{
	
	String siteURL;
	
	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_dados);
        
        Bundle b = getIntent().getExtras();
        String status = b.getString("status");
       
        siteURL = "http://www.musimania.com.br/mobile_index_token_b64bd5a1da6df03bb437d6e860e7ffc9a4c580e0_"+status;
        
        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
         }
        
         if(Utilidades.verificaConexao(this)){
        	 new MyAsyncTask().execute();
         }else{
        	 AlertDialog.Builder builder = new AlertDialog.Builder(
						ListaDadosActivity.this).setTitle("Aten��o")
						.setMessage("� necess�rio conex�o com internet para acessar essas inform��es...")
						.setPositiveButton("OK", null);
				builder.create().show();
         }
         
        	 
         
    }
	
	 private class MyAsyncTask extends AsyncTask<String, Void, List<Post>>{

	    	ProgressDialog dialog;
	    	
	    	@Override
			protected void onPreExecute() {
				super.onPreExecute();
				dialog = ProgressDialog.show(ListaDadosActivity.this, "Aguarde",
						"Baixando DADOS, Por Favor Aguarde...");
			}
	    	
			@Override
			protected List<Post> doInBackground(String... params) {
				
				
				HttpClient httpclient = new DefaultHttpClient();
				HttpGet httpget = new HttpGet(siteURL);

				try {
					HttpResponse response = httpclient.execute(httpget);

					HttpEntity entity = response.getEntity();

					if (entity != null) {
						InputStream instream = entity.getContent();

						String json = Utilidades.toString(instream);
						instream.close();
						
						List<Post> posts = getPosts(json);
						
						return posts;
					}
				} catch (Exception e) {
					Log.e("Sytesk", "Falha ao acessar Web service", e);
				}
				return null;
			}
			
			@Override
			protected void onPostExecute(final List<Post> result) {
				super.onPostExecute(result);
				dialog.dismiss();
				
				
				if(result.size() > 0){
					
					final ListView listview = (ListView) findViewById(R.id.listView1);
					final AdaptadorArray adapter = new AdaptadorArray(ListaDadosActivity.this, result);
				    listview.setAdapter(adapter);
					
				    
				    listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
				    	public void onItemClick(AdapterView<?> parent, final View view,
				  	          int position, long id) {
				    		
				    		
							Post postAtual = result.get(position);
							
							Intent myIntent = new Intent(view.getContext(),PopupActivity.class);
							
							Bundle b = new Bundle();
							b.putSerializable("postAtual", postAtual);
							myIntent.putExtras(b); 
							
							startActivityForResult(myIntent, 0);
							
				    	}
				    });
				    
				}else{
					AlertDialog.Builder builder = new AlertDialog.Builder(
							ListaDadosActivity.this).setTitle("Aten��o")
							.setMessage("N�o foi possivel acessar essas inform��es...")
							.setPositiveButton("OK", null);
					builder.create().show();
				}
				
				
			}
			
			private List<Post> getPosts(String jsonString) {

				List<Post> posts = new ArrayList<Post>();
				JSONObject jsonObject;
				
				try {
					
					jsonObject = new JSONObject(jsonString);
					
					JSONArray dados = (JSONArray) jsonObject.get("dados");
					
					JSONObject linha;
					
					for (int i = 0; i < dados.length(); i++) {
						linha = new JSONObject(dados.getString(i));
						
						
						Post post = new Post();

						post.setTitulo(linha.getString("titulo"));
						post.setData(linha.getString("data"));
						post.setIdConteudo(Integer.parseInt(linha.getString("id_conteudo")));
						post.setTexto(linha.getString("texto"));
						post.setTruncate(linha.getString("truncate"));
						
						JSONArray fotos = (JSONArray)linha.get("images");
						
						List<String> arquivo = new ArrayList<String>();
						List<String> arquivoMini= new ArrayList<String>();
						
						JSONObject linhaFoto;
						for (int j = 0; j < fotos.length(); j++) {
							linhaFoto = new JSONObject(fotos.getString(j));
							
							
								arquivo.add(linhaFoto.getString("arquivo"));
								arquivoMini.add(linhaFoto.getString("arquivo_mini"));
								
						}	
						
						post.setArquivo(arquivo);
						post.setArquivoMini(arquivoMini);
						
						posts.add(post);
					}
					

					
				} catch (Exception e) {
					Log.e("Sytesk", "Erro no parsing do JSON", e);
				}

				return posts;
			}
			
	    	
	 }
	 
	 
}


