package br.com.musinamia;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

class Post implements Serializable{
	
	private String site;
	private int idConteudo;
	private String titulo;
	private String data;
	private String texto;
	private List<String> arquivo;
	private List<String> arquivoMini;
	private String truncate;
	
	private static final long serialVersionUID = 4645;


	public Post() {
		site = "http://www.musimania.com.br";
	}
	
	
	public List<String> getArquivoLimpo() {
		return arquivo;
	}
	
	public List<String> getArquivo() {
		List<String> saida = new ArrayList<String>();
		
		for (String item : arquivo) {
			saida.add(filtraNomeArquivo(item));
		}
		
		return saida;
	}

	public List<String> getArquivoMiniLimpo() {
		return arquivoMini;
	}

	public List<String> getArquivoMini() {
		
		List<String> saida = new ArrayList<String>();
		
		for (String item : arquivoMini) {
			saida.add(filtraNomeArquivo(item));
		}
		
		return saida;
	}



	public int getIdConteudo() {
		return idConteudo;
	}



	public void setIdConteudo(int idConteudo) {
		this.idConteudo = idConteudo;
	}



	public String getTitulo() {
		return titulo;
	}



	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}



	public String getData() {
		return data;
	}



	public void setData(String data) {
		this.data = data;
	}



	public String getTexto() {
		return texto;
	}



	public void setTexto(String texto) {
		this.texto = texto;
	}



	public String getTruncate() {
		return truncate;
	}



	public void setTruncate(String truncate) {
		this.truncate = truncate;
	}


	

	public void setArquivo(List<String> arquivo) {
		this.arquivo = arquivo;
	}



	public void setArquivoMini(List<String> arquivoMini) {
		this.arquivoMini = arquivoMini;
	}



	public String getArquivoPosition(int position) {
		
		String arquivoLocal = this.arquivo.get(position);
		
		return filtraNomeArquivo(arquivoLocal);
	}
	
	private String filtraNomeArquivo(String arquivoLocal){
		
		if(!arquivoLocal.startsWith(site) && !arquivoLocal.equals("")){
			arquivoLocal = site+"/foto_conteudo/"+arquivoLocal;
		}
		
		return arquivoLocal;
	}
	
	public String getArquivoMiniPosition(int position) {
		
		String arquivoLocal = this.arquivoMini.get(position);
		
		return filtraNomeArquivo(arquivoLocal);
	}
	
	
}
