package br.com.musinamia;

import java.util.List;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

public class PopupActivity extends Activity{
	
   @SuppressLint("CutPasteId")
protected void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.popup_layout);
		
		final Bundle b = getIntent().getExtras();
        Post postAtual = (Post)b.getSerializable("postAtual");
		
        
        if(postAtual.getTitulo().equals("Mensagem")){
        	String msg = Utilidades.getConteudoArquivo(this, "arquivo.txt");
        	postAtual.setTexto(msg);
        }
        
        TextView txtPopup = (TextView)findViewById(R.id.textViewPopup);
		TextView txtPopupNome = (TextView)findViewById(R.id.textViewPopupNome);
        
		txtPopup.setText(Html.fromHtml(postAtual.getTexto()));
		txtPopupNome.setText(Html.fromHtml(postAtual.getTitulo()));
		
		ImageButton btnDismiss = (ImageButton)findViewById(R.id.btnPouupFechar);
		
		btnDismiss.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v2) {
				finish();
			}
		});
		
		ImageView ivAtual = (ImageView)findViewById(R.id.imageView1);
		ivAtual.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v2) {
				
				Intent myIntent = new Intent(v2.getContext(),DetalheActivity.class);
				
				Bundle b2 = new Bundle();
				b2.putSerializable("postAtual", b.getSerializable("postAtual"));
				myIntent.putExtras(b2); 
				
				startActivityForResult(myIntent, 0);
				
			}
		});
		
		
		PopupTask task = new PopupTask();
		
		Context contexto = getBaseContext();
		
		
		task.postAtual = postAtual;
		task.contexto = contexto;
		task.execute();
		
	}
   	
   private class PopupTask extends AsyncTask<String, Void, List<Post>>{
		
	   	ProgressDialog dialog;
	   	
	   	Post postAtual;
	   	
	   	Context contexto;
	   	
	   	@Override
			protected void onPreExecute() {
				super.onPreExecute();
				dialog = ProgressDialog.show(PopupActivity.this, "Aguarde",
						"Carregando imagens, Por Favor Aguarde...");
				
			}
	   	
			@Override
			protected List<Post> doInBackground(String... params) {
				
				try {
					
					
					ImageView iv = (ImageView)findViewById(R.id.imageView1);
					
					String arquivoMini = postAtual.getArquivoMiniPosition(0);
				    
					if(!arquivoMini.isEmpty()){
						Bitmap image = Utilidades.downloadBitmap(arquivoMini,contexto);
						iv.setImageBitmap(image);
					}
					
					
				} catch (Exception e) {
					Log.e("Sytesk", "Erro ao tentar exbir detalhes", e);
				}
				
				return null;
			}
			
			@Override
			protected void onPostExecute(final List<Post> result) {
				super.onPostExecute(result);
				dialog.dismiss();
				
				
				
			}
	}
	
}
