package br.com.musinamia;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        TextView textSytesk = (TextView)findViewById(R.id.textViewSytesk); 
        
        textSytesk.setText(
                Html.fromHtml(
                    "<a href=\"http://www.sytesk.com.br\">Sytesk</a> "));
        textSytesk.setMovementMethod(LinkMovementMethod.getInstance());
        
        
        ImageView ivProfessores = (ImageView) findViewById(R.id.imageProfessores);
        ImageView ivDicas = (ImageView) findViewById(R.id.imageDicas);
        ImageView ivFotos = (ImageView) findViewById(R.id.imageFotos);
        ImageView ivCO = (ImageView) findViewById(R.id.imageCO);
        ImageView ivCP = (ImageView) findViewById(R.id.imageCP);
        
        ivCP.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent myIntent = new Intent(v.getContext(),ListaDadosActivity.class);
				
				Bundle b = new Bundle();
				b.putString("status", "menu_CP");
				myIntent.putExtras(b); 
				
				startActivityForResult(myIntent, 0);
				
			}
		});
        
        ivCO.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent myIntent = new Intent(v.getContext(),ListaDadosActivity.class);
				
				Bundle b = new Bundle();
				b.putString("status", "menu_CO");
				myIntent.putExtras(b); 
				
				startActivityForResult(myIntent, 0);
				
			}
		});
        
        ivFotos.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent myIntent = new Intent(v.getContext(),ListaDadosActivity.class);
				
				Bundle b = new Bundle();
				b.putString("status", "tipo_F");
				myIntent.putExtras(b); 
				
				startActivityForResult(myIntent, 0);
				
			}
		});
        
        ivDicas.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent myIntent = new Intent(v.getContext(),ListaDadosActivity.class);
				
				Bundle b = new Bundle();
				b.putString("status", "tipo_D");
				myIntent.putExtras(b); 
				
				startActivityForResult(myIntent, 0);
				
			}
		});
        
        ivProfessores.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent myIntent = new Intent(v.getContext(),ListaDadosActivity.class);
				
				Bundle b = new Bundle();
				b.putString("status", "tipo_M");
				myIntent.putExtras(b); 
				
				startActivityForResult(myIntent, 0);
				
			}
		});
        
        
        String opcao = Utilidades.getConteudoArquivo(this, "opcao.txt");
        
        //se for executar na primeira vez, abre o popup
        if(opcao.isEmpty()){
        	Intent myIntent = new Intent(this,PopupInicialActivity.class);
    		
    		Bundle b = new Bundle();
    		myIntent.putExtras(b); 
    		
    		startActivityForResult(myIntent, 0);
        }
        
    }
    
    

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }
    
}
