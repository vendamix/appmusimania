package br.com.musinamia;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class PopupInicialActivity extends Activity {
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.popup_inicial_layout);
		

		
	}
	
	public void souAluno(View view){
		
		Intent i= new Intent(this, Notifica.class);
	    
	    startService(i);
	     
	    int flag = i.getFlags();
		
	    Utilidades.escrecerArquivo(this, "opcao.txt", "A");
	    
		finish();
	}
	
	public void naoSouAluno(View view){
		
		Utilidades.escrecerArquivo(this, "opcao.txt", "N");
		
		finish();
	}
	
}
