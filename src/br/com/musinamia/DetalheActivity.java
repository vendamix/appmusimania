package br.com.musinamia;

import java.util.List;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Gallery;
import android.widget.ImageButton;


public class DetalheActivity extends Activity{
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.detalhe_layout);
		
		Bundle b = getIntent().getExtras();
        Post postAtual = (Post)b.getSerializable("postAtual");
        
		ImageButton btnDismiss = (ImageButton)findViewById(R.id.btnPouupFecharDetalhe);
		
		btnDismiss.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v2) {
				finish();
			}
		});
		
		
		Context contexto = getBaseContext();
	
		DetalheTask task = new DetalheTask();
		
		task.postAtual = postAtual;
		task.contexto = contexto;
		task.execute();
	}
	
	
	private class DetalheTask extends AsyncTask<String, Void, List<Post>>{
	
	   	ProgressDialog dialog;
	   	
	   	Post postAtual;
	   	
	   	Context contexto;
	   	
	   	@Override
			protected void onPreExecute() {
				super.onPreExecute();
				dialog = ProgressDialog.show(DetalheActivity.this, "Aguarde",
						"Carregando imagens, Por Favor Aguarde...");
			}
	   	
			@Override
			protected List<Post> doInBackground(String... params) {
				
				try {
					
					AdaptadorImagem adapter = new AdaptadorImagem(this.contexto);
					adapter.setPost(postAtual);
					((Gallery) findViewById(R.id.gallery1))
					.setAdapter(adapter);
					
				} catch (Exception e) {
					Log.e("Sytesk", "Erro ao tentar exbir detalhes", e);
				}
				
				return null;
			}
			
			@Override
			protected void onPostExecute(final List<Post> result) {
				super.onPostExecute(result);
				dialog.dismiss();
				
				
				
			}
	}
}
